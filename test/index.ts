import FcFormattedOutput from '../src';

const output = new FcFormattedOutput();

console.log(output.get('domain', 'sss.sss'));
console.log(output.set('function', 'function-name'));
console.log(output.create('service', 'function-name'));
console.log(output.update('trigger', 'trigger-name'));
console.log(output.remove('service', 'service-name'));
console.log(output.warn('customDomain', 'default is auto'));
console.log(output.warn('customDomain', 'default is auto', 'you can see abc'));
console.log(output.nextStep(['Invoke remote function: s invoke', 'Remove service: s remove service']));
console.log(output.using('region', 'cn-shanghai'));
console.log(output.check('serivce', 'serivce-name'));
console.log(output.retry('function', 'create', 'myfunction'));
console.log(output.retry('function', 'create', 'myfunction', 2));