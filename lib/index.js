"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var firstUpperCase = function (str) { return str === null || str === void 0 ? void 0 : str.toString().replace(/^( |^)[a-z]/g, function (L) { return L.toUpperCase(); }); };
var FcFormattedOutput = /** @class */ (function () {
    function FcFormattedOutput() {
    }
    FcFormattedOutput.prototype.get = function (type, details) {
        // E.g.: Getting domain: todolist.todolist.*****.cn-hangzhou.fc.devsapp.net
        return "Getting " + type + ": " + details;
    };
    FcFormattedOutput.prototype.set = function (type, details) {
        // E.g.: Setting domain: todolist.todolist.*****.cn-hangzhou.fc.devsapp.net
        return "Setting " + type + ": " + details;
    };
    FcFormattedOutput.prototype.create = function (type, details) {
        // E.g.: Creating serivce: my-test-service
        return "Creating " + type + ": " + details;
    };
    FcFormattedOutput.prototype.update = function (type, details) {
        // E.g.: Updating function: my-test-function
        return "Updating " + type + ": " + details;
    };
    FcFormattedOutput.prototype.remove = function (type, details) {
        // E.g.: Removing function: my-test-function
        return "Removing " + type + ": " + details;
    };
    FcFormattedOutput.prototype.warn = function (type, details, description) {
        // E.g.:
        // Reminder customDomain: default is auto
        // Reminder deploy type: sdk (Switch command [s cli fc-default set deploy-type pulumi])
        return "Reminder " + type + ": " + details + (description ? "(" + description + ")" : '');
    };
    FcFormattedOutput.prototype.nextStep = function (step) {
        // E.g.:
        //   Tips for next step:
        //     * Invoke remote function: s invoke
        //     * Remove service: s remove service
        return "\nTips for next step:" + step.map(function (item) { return "\n* " + item; }) + "\n";
    };
    FcFormattedOutput.prototype.using = function (type, details) {
        // E.g.: Using region: cn-hangzhou
        return "Using " + type + ": " + details;
    };
    FcFormattedOutput.prototype.check = function (type, details) {
        // E.g.: Checking Serivce my-service exists
        return "Checking " + firstUpperCase(type) + " " + details + " exists";
    };
    FcFormattedOutput.prototype.retry = function (type, action, details, times) {
        // E.g.: 
        // Retrying function: create myfunction
        // Retrying function: create myfunction, retry 1 time
        return "Retrying " + type + ": " + action + " " + details + (times ? ", retry " + times + " time" : '');
    };
    return FcFormattedOutput;
}());
exports.default = FcFormattedOutput;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxJQUFNLGNBQWMsR0FBRyxVQUFDLEdBQVcsV0FBSyxHQUFHLGFBQUgsR0FBRyx1QkFBSCxHQUFHLENBQUUsUUFBUSxHQUFHLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQWYsQ0FBZSxJQUFDLENBQUE7QUFFdkc7SUFBQTtJQXlEQSxDQUFDO0lBeERRLCtCQUFHLEdBQVYsVUFBVyxJQUFZLEVBQUUsT0FBZTtRQUN0QywyRUFBMkU7UUFDM0UsT0FBTyxhQUFXLElBQUksVUFBSyxPQUFTLENBQUM7SUFDdkMsQ0FBQztJQUVNLCtCQUFHLEdBQVYsVUFBVyxJQUFZLEVBQUUsT0FBZTtRQUN0QywyRUFBMkU7UUFDM0UsT0FBTyxhQUFXLElBQUksVUFBSyxPQUFTLENBQUM7SUFDdkMsQ0FBQztJQUVNLGtDQUFNLEdBQWIsVUFBYyxJQUFZLEVBQUUsT0FBZTtRQUN6QywwQ0FBMEM7UUFDMUMsT0FBTyxjQUFZLElBQUksVUFBSyxPQUFTLENBQUM7SUFDeEMsQ0FBQztJQUVNLGtDQUFNLEdBQWIsVUFBYyxJQUFZLEVBQUUsT0FBZTtRQUN6Qyw0Q0FBNEM7UUFDNUMsT0FBTyxjQUFZLElBQUksVUFBSyxPQUFTLENBQUM7SUFDeEMsQ0FBQztJQUVNLGtDQUFNLEdBQWIsVUFBYyxJQUFZLEVBQUUsT0FBZTtRQUN6Qyw0Q0FBNEM7UUFDNUMsT0FBTyxjQUFZLElBQUksVUFBSyxPQUFTLENBQUM7SUFDeEMsQ0FBQztJQUVNLGdDQUFJLEdBQVgsVUFBWSxJQUFZLEVBQUUsT0FBZSxFQUFFLFdBQW9CO1FBQzdELFFBQVE7UUFDUix5Q0FBeUM7UUFDekMsdUZBQXVGO1FBQ3ZGLE9BQU8sY0FBWSxJQUFJLFVBQUssT0FBTyxJQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsTUFBSSxXQUFXLE1BQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUM7SUFDaEYsQ0FBQztJQUVNLG9DQUFRLEdBQWYsVUFBZ0IsSUFBYztRQUM1QixRQUFRO1FBQ1Isd0JBQXdCO1FBQ3hCLHlDQUF5QztRQUN6Qyx5Q0FBeUM7UUFDekMsT0FBTywwQkFBd0IsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLElBQUksSUFBSSxPQUFBLFNBQU8sSUFBTSxFQUFiLENBQWEsQ0FBQyxPQUFJLENBQUM7SUFDckUsQ0FBQztJQUVNLGlDQUFLLEdBQVosVUFBYSxJQUFZLEVBQUUsT0FBZTtRQUN4QyxrQ0FBa0M7UUFDbEMsT0FBTyxXQUFTLElBQUksVUFBSyxPQUFTLENBQUM7SUFDckMsQ0FBQztJQUVNLGlDQUFLLEdBQVosVUFBYSxJQUFZLEVBQUUsT0FBZTtRQUN4QywyQ0FBMkM7UUFDM0MsT0FBTyxjQUFZLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBSSxPQUFPLFlBQVMsQ0FBQztJQUM5RCxDQUFDO0lBRU0saUNBQUssR0FBWixVQUFhLElBQVksRUFBRSxNQUFjLEVBQUUsT0FBZSxFQUFFLEtBQWM7UUFDeEUsU0FBUztRQUNULHVDQUF1QztRQUN2QyxxREFBcUQ7UUFDckQsT0FBTyxjQUFZLElBQUksVUFBSyxNQUFNLFNBQUksT0FBTyxJQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsYUFBVyxLQUFLLFVBQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFFLENBQUM7SUFDekYsQ0FBQztJQUNILHdCQUFDO0FBQUQsQ0FBQyxBQXpERCxJQXlEQyJ9