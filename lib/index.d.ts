export default class FcFormattedOutput {
    get(type: string, details: string): string;
    set(type: string, details: string): string;
    create(type: string, details: string): string;
    update(type: string, details: string): string;
    remove(type: string, details: string): string;
    warn(type: string, details: string, description?: string): string;
    nextStep(step: string[]): string;
    using(type: string, details: string): string;
    check(type: string, details: string): string;
    retry(type: string, action: string, details: string, times?: number): string;
}
